from flask import request, session, url_for
from flask_mail import Mail, Message


#from admin.models import Setting
import views.models as vm
import settings
import smtplib
from smtplib import SMTPAuthenticationError
from passlib.hash import sha256_crypt
from passlib.hash import django_pbkdf2_sha256
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

mail = Mail()
unsubscribe_message = '\n\nTo unsubscribe from these alerts, reply with UNSUBSCRIBE in the subject line.'


def advisor_client_create_email(user, hashes):

    link = url_for('simple.activate_account', hashes=hashes, _external=True)
    email_records = vm.Emails.query.filter_by(
        email_slug="advisor_client").first()
    # print user.userid.user_id
    #userprofile = vm.UserProfile.query.filter_by(user_id=user.userid.user_id).first()
    #userprofile.activation_code = hashes
    # db_session.commit()
    print(email_records.email_subject)
    message = email_records.email_message
    message = message.replace("<-FIRSTNAME->", user.first_name)
    message = message.replace("<-LASTNAME->", user.last_name)
    message = message.replace("<-ADVISORNAME->", str(user.userid.advisor_id))
    message = message.replace("<-ACTIVATIONLINK->", link)

    send_email(email_records.email_subject, [user.email], message)

def password_reset(user, hashes):
    print(hashes)
    link = url_for('simple.reset_password', hashes=hashes, _external=True)
    email_records = vm.Emails.query.filter_by(email_slug="forgot").first()
    print(email_records.email_subject)
    message = email_records.email_message
    message = message.replace("<-FIRSTNAME->", user.first_name)
    message = message.replace("<-LASTNAME->", user.last_name)
    #message = message.replace("<-PASSWORDRESETLINK->", link)
    message = message.replace("<-PASSWORDTEMP->", hashes)
    send_email(email_records.email_subject, [user.email], message)


def notify_url_email(form_fields):
    cc = []
    bcc = []
    msg = Message(
        body=str(form_fields),
        subject="paypal transaction",
        recipients=["hughson.simon@gmail.com"],
        cc=cc,
        bcc=bcc +
        ['hughson.simon@gmail.com'])
    mail.send(msg)


def user_register_email(user, hashes):

    link = url_for('simple.activate_account', hashes=hashes, _external=True)
    email_records = vm.Emails.query.filter_by(email_slug="register").first()
    print(email_records.email_subject)
    message = email_records.email_message
    message = message.replace("<-FIRSTNAME->", user.first_name)
    message = message.replace("<-LASTNAME->", user.last_name)
    #message = message.replace("<-ADVISORNAME->", user.userid.advisor_id)
    message = message.replace("<-ACTIVATIONLINK->", link)
    send_email(email_records.email_subject, [user.email], message)

def user_manual_register_email(user, password, hashes):

    link = url_for('simple.activate_account', hashes=hashes, _external=True)
    email_records = vm.Emails.query.filter_by(email_slug="manual_register").first()
    print(email_records.email_subject)
    message = email_records.email_message
    message = message.replace("<-FIRSTNAME->", user.first_name)
    message = message.replace("<-LASTNAME->", user.last_name)
    message = message.replace("<-PASSWORD->", password)
    message = message.replace("<-ACTIVATIONLINK->", link)
    send_email(email_records.email_subject, [user.email], message)


def report_paypal_email(
        email_type,
        user,
        paypa_txn_id,
        status,
        sub_id,
        reason=''):
    print("sending paypal mail")
    email_records = vm.Emails.query.filter_by(email_slug=email_type).first()
    print(email_records.email_subject)
    message = email_records.email_message
    message = message.replace("<-FIRSTNAME->", user.first_name)
    message = message.replace("<-LASTNAME->", user.last_name)
    #message = message.replace("<-ADVISORNAME->", user.userid.advisor_id)
    message = message.replace("<-SUBSCRIPTIONID->", str(sub_id))

    if reason != "":
        message = message.replace("<-PAYPALREASON->", str(status))
    else:
        message = message.replace("<-PAYPALTRANSID->", str(paypa_txn_id))
        message = message.replace("<-PAYPALSTATUS->", str(status))

    print(user.email)
    send_email(email_records.email_subject, [user.email], message)


def test_email(subject, user, hashes):

    link = url_for('simple.activate_account', hashes=hashes, _external=True)
    email_records = vm.Emails.query.filter_by(email_slug="register").first()
    print(email_records.email_subject)
    message = email_records.email_message
    message = message.replace("<-FIRSTNAME->", user.first_name)
    message = message.replace("<-LASTNAME->", user.last_name)
    #message = message.replace("<-ADVISORNAME->", user.userid.advisor_id)
    message = message.replace("<-ACTIVATIONLINK->", link)

    print(link)
    print(hashes)
    print(message)
    print(user.email)
    send_email(email_records.email_subject, [user.email], message)


def replace_variables(message, variables):
    for key, value in variables.iteritems():
        message = message.replace('[%s]' % key, value)
    return message



mailServer = smtplib.SMTP(settings.MAIL_SERVER, settings.MAIL_PORT)
#msg = MIMEMultipart('mixed')


def send_email(
        subject,
        recipients,
        message,
        cc=[],
        bcc=[],
        attachment_filename=None,
        attachment_content_type='application/octet-stream',
        attachment_data=None):
    if not recipients and cc:
        recipients = cc
        cc = []
    #msg = Message(body=message, subject=subject, recipients=recipients, cc=cc, bcc=bcc + ['hughson.simon@gmail.com',  settings.MAIL_DEFAULT_SENDER])
    msg = Message(
        body=message,
        subject=subject,
        recipients=recipients,
        cc=cc,
        bcc=bcc,
        sender=settings.MAIL_DEFAULT_SENDER)
    if attachment_data:
        msg.attach(
            attachment_filename,
            attachment_content_type,
            attachment_data)
    mail.send(msg)
    '''
    sender = settings.MAIL_DEFAULT_SENDER
    msg = MIMEMultipart('mixed')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = recipients
    html_message = MIMEText(message, 'plain')
    msg.attach(html_message)
    message1 = """\
        From: %s
        To: %s
        Subject: %s
        %s
        """ % (sender, ", ".join(recipients), subject, message)
    mailServer.set_debuglevel(1)
    #mailServer.ehlo()
    #mailServer.starttls()
    #mailServer.ehlo()

    import textwrap
    message2 = textwrap.dedent("""\
        From: %s
        To: %s
        Subject: %s
        %s
        """ % (settings.MAIL_DEFAULT_SENDER, ", ".join("hughson.simon@gmail.com"), subject, message))
    try:
        mailServer.login(settings.MAIL_USERNAME, settings.MAIL_PASSWORD)
        mailServer.sendmail(settings.MAIL_DEFAULT_SENDER, "hughson.simon@gmail.com", message2)
    except SMTPAuthenticationError:
        print 'SMTPAuthenticationError'
    finally:
        mailServer.quit()
        mailServer.close()

    #import smtplib
    import email.utils
    #from email.mime.text import MIMEText

    # Create the message
    msg1 = MIMEText(message)
    msg1['To'] = email.utils.formataddr(('Recipient', 'hughson.simon@gmail.com'))
    msg1['From'] = email.utils.formataddr(('Author', settings.MAIL_DEFAULT_SENDER))
    msg1['Subject'] = subject

    server = smtplib.SMTP('mail')
    server.set_debuglevel(True) # show communication with the server
    try:
        server.sendmail("hughson.simon@gmail.com", ['hughsons.apps@gmail.com'], msg1.as_string())
    finally:
        server.quit()
    '''


def error_email(code=""):
    import traceback
    message = ('URL: %s' % request.path) + '\n\n'
    message += ''.join(traceback.format_exc())
    print('RRR Server Error ::==:: %s' % message)
    send_email(
        'RRR Server Error' +
        str(code),
        ['hughson.simon@gmail.com'],
        message)
