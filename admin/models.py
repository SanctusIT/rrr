from datetime import date, datetime, timedelta
import json
from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.ext.mutable import Mutable
from sqlalchemy.orm import backref, relationship
import sqlalchemy.types as types

from database import Base


def accessory_status(date_val, warning_days=60, critical_days=30):
    if not date_val or date_val > date.today() + timedelta(days=warning_days):
        return 'ok'
    if date_val > date.today() + timedelta(days=critical_days):
        return 'warning'
    if date_val > date.today():
        return 'critical'
    return 'expired'


class MutableDict(Mutable, dict):
    @classmethod
    def coerce(cls, key, value):
        """
        Convert plain dictionaries to MutableDict.
        """

        if not isinstance(value, MutableDict):
            if isinstance(value, dict):
                return MutableDict(value)
            return Mutable.coerce(key, value)
        else:
            return value

    def __setitem__(self, key, value):
        """
        Detect dictionary set events and emit change events.
        """

        dict.__setitem__(self, key, value)
        self.changed()

    def __delitem__(self, key):
        """
        Detect dictionary del events and emit change events.
        """

        dict.__delitem__(self, key)
        self.changed()


class JsonField(types.TypeDecorator):
    impl = types.Text

    def process_bind_param(self, value, engine):
        if value is None:
            return None
        return json.dumps(value)

    def process_result_value(self, value, engine):
        if value is None:
            return None
        return json.loads(value)


MutableDict.associate_with(JsonField)


class StockMoments(Base):
    """
    Data model for the betas and moments for a given company.
    """
    __tablename__ = 'analysis_stockmoments'
    #company = models.OneToOneField(Company)
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('stockdata_company.id'))
    date = Column(String(46))
    forecast = Column(String(46))
    expdivyield = Column(String(46))
    betafactor1 = Column(String(46))
    betafactor2 = Column(String(46))
    betafactor3 = Column(String(46))
    betafactor4 = Column(String(46))
    betafactor5 = Column(String(46))
    betafactor6 = Column(String(46))
    betafactor7 = Column(String(46))
    resmoment2 = Column(String(46))
    resmoment3 = Column(String(46))
    resmoment4 = Column(String(46))
    totretmoment2 = Column(String(46))
    totretmoment3 = Column(String(46))
    totretmoment4 = Column(String(46))
    standarddeviation = Column(String(46))
    standardskew = Column(String(46))
    standardkurtosis = Column(String(46))
    companyid = relationship('StockCompanies', uselist=False)


class Metrics(Base):
    """
    Data model for storing the Cokurtosis, Coskewness and Covariance matrices.
    """
    __tablename__ = 'analysis_metrics'
    COKUR = 'COKUR'
    COSKEW = 'COSKEW'
    COVAR = 'COVAR'

    METRIC_TYPE_CHOICES = (
        (COKUR, 'Cokurtosis'),
        (COSKEW, 'Coskew'),
        (COVAR, 'Covariance'),
    )
    id = Column(Integer, primary_key=True)
    type = Column(String(46))
    # used to store metric data as a list of comma separated values
    data = Column(Text)
    # number of rows (= columns) in the n-dimensional matrix represented by
    # the data field
    order = Column(Integer)
    #updated_on = Column(String(56))


class StockCompanies(Base):
    """
    Data model for the betas and moments for a given company.
    """
    __tablename__ = 'stockdata_company'
    #company = models.OneToOneField(Company)
    id = Column(Integer, primary_key=True)
    symbol = Column(String(46))
    name = Column(String(46))
    category = Column(String(46))
    asset_class = Column(String(46))
    is_traded = Column(Boolean, default=True)


class StockTimestamp(Base):
    __tablename__ = 'stockdata_timestamps'
    id = Column(Integer, primary_key=True)
    name = Column(String(46))
    last_updated = Column(String(56))


class SoftwareSettingss(Base):
    __tablename__ = 'software_settings'
    id = Column(Integer, primary_key=True)
    free_slots = Column(Integer)
    iriskware_upload = Column(Integer)
    minimum_postcode = Column(String(46))
    maximum_postcode = Column(String(56))
    cooltrader_username = Column(String(56))
    cooltrader_password = Column(String(56))
    paypal_email = Column(String(56))
    settings_slug = Column(String(45))
    site_address = Column(Text)
    site_city = Column(String(45))
    site_state = Column(String(45))
    site_email = Column(String(45))
    site_zipcode = Column(String(45))
    site_phone = Column(String(45))
    idle_timeout = Column(String(10))


def status_func(attr_name):
    def status_method(self):
        attr = getattr(self, attr_name)
        return accessory_status(
            attr,
            self.STATUS_WARNING_DAYS,
            self.STATUS_CRITICAL_DAYS)
    return status_method



class StatusMixin(object):
    status = property(status_func('expiration_date'))


class PasswordReset(Base):
    __tablename__ = 'password_resets'

    id = Column(String(64), primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))


class Setting(Base):
    __tablename__ = 'settings'

    name = Column(String(32), primary_key=True)
    value = Column(Text, nullable=False)


class SiteMedia(Base):
    __tablename__ = 'content_sitemedia'
    id = Column(Integer, primary_key=True)
    media_path = Column(Text)
    media_content = Column(Text)
    media_status = Column(Boolean, default=True)
    media_category = Column(String(26))
    created_on = Column(String(54))
    media_title = Column(String(220))
