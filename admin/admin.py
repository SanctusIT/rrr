from datetime import datetime
from flask import Blueprint, flash, request, render_template, session, url_for
from database import db_session
from decorators import require_permission
from helpers import date_from_form_field, time_from_form_field, redirect_next
from flask_login import login_user, logout_user, current_user, login_required
from views.forms import LoginForm, RegistrationForm
from sqlalchemy.orm.exc import NoResultFound
from views.models import *
from admin.models import *
from passlib.hash import django_pbkdf2_sha256
from sqlalchemy import desc
from flask import jsonify
import uuid
import sys
from csv import DictReader
import os
import csv
import datetime as dti
from werkzeug import secure_filename
import helpers as hlp
import datetime as dttim
#from .getstockprices import StockCommand
from hashlib import sha256
import emails
import random
from hashlib import sha256
import hashlib

ALLOWED_EXTENSIONS = ['csv', 'pdf', 'png', 'jpg', 'jpeg', 'gif']

blueprint = Blueprint('events', __name__, url_prefix='/admin')
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, '../static/sitemedia/')


@blueprint.route('/')
def home():
    return redirect_next("/admin/login")
    # return render_template('index.html')


@blueprint.route('/pages')
@login_required
def content_pages():
    if request.args.get('slug') == "":
        slug = "home"
    else:
        if 'slug' in request.args:
            slug = request.args.get('slug')
        else:
            slug = 'home'
    entity_records = Pages.query.filter_by(page_slug=slug).first()
    header_block = PageBlocks.query.filter_by(block_slug="header").first()
    services_iriskonline = PageBlocks.query.filter_by(block_slug="services_iriskonline").first()
    services_iriskaware = PageBlocks.query.filter_by(block_slug="services_iriskaware").first()
    services_iriskwatch = PageBlocks.query.filter_by(block_slug="services_iriskwatch").first()
    banners = PageBlocks.query.filter_by(block_slug="banners").all()
    evidence = PageBlocks.query.filter_by(block_slug="evidence").all()
    footer = PageBlocks.query.filter_by(block_slug="footer").first()
    print(entity_records.page_name)
    return render_template(
        'admin/pages.html',
        entity_records=entity_records,
        header_block=header_block,
        services_iriskonline=services_iriskonline,
        services_iriskwatch=services_iriskwatch,
        services_iriskaware=services_iriskaware,
        banners=banners,
        evidence=evidence,
        footer=footer)

@blueprint.route('/aboutus')
@login_required
def aboutus_page():
    if request.args.get('slug') == "":
        slug = "aboutus"
    else:
        if 'slug' in request.args:
            slug = request.args.get('slug')
        else:
            slug = 'aboutus'
    entity_records = Pages.query.filter_by(page_slug=slug).first()
    return render_template('admin/aboutus.html', entity_records=entity_records)

@blueprint.route('/privacy')
@login_required
def privacy_page():
    if request.args.get('slug') == "":
        slug = "privacy"
    else:
        if 'slug' in request.args:
            slug = request.args.get('slug')
        else:
            slug = 'privacy'
    entity_records = Pages.query.filter_by(page_slug=slug).first()
    return render_template('admin/privacy.html', entity_records=entity_records)

@blueprint.route('/get_started')
@login_required
def get_started():
    if request.args.get('slug') == "":
        slug = "get_started"
    else:
        if 'slug' in request.args:
            slug = request.args.get('slug')
        else:
            slug = 'get_started'
    entity_records = Pages.query.filter_by(page_slug=slug).first()
    return render_template('admin/get_started.html', entity_records=entity_records)

@blueprint.route('/terms')
@login_required
def terms_page():
    if request.args.get('slug') == "":
        slug = "terms"
    else:
        if 'slug' in request.args:
            slug = request.args.get('slug')
        else:
            slug = 'terms'
    entity_records = Pages.query.filter_by(page_slug=slug).first()
    return render_template('admin/terms.html', entity_records=entity_records)





@blueprint.route('/adminemails')
@login_required
def admin_emails():
    if request.args.get('slug') == "":
        slug = "register"
    else:
        if 'slug' in request.args:
            slug = request.args.get('slug')
        else:
            slug = 'register'
    #print slug
    #print "*" * 40
    entity_records = Emails.query.filter_by(email_slug=slug).first()
    email_records = Emails.query.all()
    return render_template(
        'admin/admin_emails.html',
        entity_records=entity_records,
        email_records=email_records)


@blueprint.route('/pricing')
@login_required
def admin_pricing():
    entity_records = PricingTable.query.all()
    return render_template('admin/pricing.html', entity_records=entity_records)

@blueprint.route('/manual_register')
@login_required
def manual_register():
    entity_records = SoftwareSettingss.query.filter_by(id="4").one()
    return render_template(
        'admin/admin_manual_register.html',
        entity_records=entity_records)

@blueprint.route('/admin_settings')
@login_required
def admin_settings():
    entity_records = SoftwareSettingss.query.filter_by(id="4").one()
    return render_template(
        'admin/admin_settings.html',
        entity_records=entity_records)

@blueprint.route('/create_faq_form')
@login_required
def create_faq_form():

    return render_template(
        'admin/admin_create_faq_form.html',
        entity_records="")

@blueprint.route('/edit_faq_form', methods=['GET'])
@login_required
def edit_faq_form():
    print(request.args.get('id'))
    if request.args.get('id'):
        result_items = Faqs.query.filter_by(id=request.args.get('id')).first()
        if result_items is None:
            print('here')
            return jsonify({'message': 'ERROR'})
        else:
            result_items = result_items
    else:
        return jsonify({'message': 'ERROR'})
    form_title = "Edit faqs"
    #table_name = request.form['table_name']
    return render_template(
        'admin/admin_edit_faq_form.html',
        item_details=result_items,
        title='Faq Table',
        form_title=form_title,
        table_name="")

@blueprint.route('/FaqEditModalView', methods=['POST'])
@login_required
def edit_faq_view_form():
    if request.method == 'POST':
        form_title = "Edit Faqs Table"
        #print request.form['table_name']
        #print request.form['id']
        if request.form['table_name'] == "faqs":
            if request.form.get('id'):
                result_items = Faqs.query.filter_by(
                    id=request.form.get('id')).first()
                if result_items is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    result_items = result_items
            else:
                return jsonify({'message': 'ERROR'})
            form_title = "Edit faqs"
            table_name = request.form['table_name']
        else:
            return jsonify({'message': 'ERROR'})
        # print result_items.id
        return render_template(
            'admin/edit_faq_modal.html',
            item_details=result_items,
            title='Faq Table',
            form_title=form_title,
            table_name=table_name)
    else:
        return jsonify({'message': 'Unsupported Request'})



@blueprint.route('/admin_tutor')
@login_required
def admin_tutor():
    user_records = TutorBlocks.query.all()
    return render_template(
        'admin/admin_tutor.html', user_records=user_records, title="Tutor")

@blueprint.route('/faqs')
@login_required
def admin_faqs():
    user_records = Faqs.query.all()
    # print user_records.count()
    #print user_records
    return render_template( 'admin/admin_faqs.html', user_records=user_records, title="FAQs")


@blueprint.route('/blogs')
@login_required
def admin_blogs():
    user_records = SiteBlogs.query.all()
    #print user_records
    return render_template('admin/admin_blogs.html', user_records=user_records, title="Blogs")


@blueprint.route('/uploadmedia', methods=['POST', 'GET'])
@login_required
def admin_sitemedia():
    if request.method == 'POST':
        dict = request.form
        for key in dict.keys():
            for value in dict.getlist(key):
                print(key, ":", value)
        # print request.files.get('file')
        print(request.files['file'])
        if request.files['file']:

            csv_file = request.files['file']
            now = datetime.now()

            f = request.files['file']
            filename = f.filename
            f.save(os.path.join(UPLOAD_FOLDER, secure_filename(filename)))

            try:
                item_record = SiteMedia(
                    created_on=now,
                    media_path=filename,
                    media_title=request.form.get('media_title'))
                db_session.add(item_record)
                db_session.commit()
                return jsonify({'message': 'SUCCESS'})
            except BaseException:
                return jsonify({'message': 'ERROR'})

        else:
            return jsonify({'message': 'ERROR'})
    user_records = SiteMedia.query.all()
    return render_template(
        'admin/site_media.html',
        user_records=user_records,
        title="Site Media")


@blueprint.route('/upload_metrics', methods=['POST', 'GET'])
@login_required
def upload_metrics():
    if request.method == 'POST':
        dict = request.form
        for key in dict.keys():
            for value in dict.getlist(key):
                print(key, ":", value)
        # print request.files.get('file')
        print(request.files['file'])
        if request.files['file']:
            if request.form.get('metric_type') == "2":
                types = "COKUR"
            elif request.form.get('metric_type') == "3":
                types = "COVAR"
            elif request.form.get('metric_type') == "4":
                types = "COSKEW"
            else:
                types = "COKUR"
            print(types)
            csv_file = request.files['file']
            now = datetime.now()
            #filename = os.path.join(app.config['UPLOAD_FOLDER'], "%s.%s" % (now.strftime("%Y-%m-%d-%H-%M-%S-%f"), file.filename.rsplit('.', 1)[1]))
            # file.save(filename)
            data_list = [x for x in csv_file if x]  # trim empty lines
            order = len(data_list)
            #print order
            values = ','.join(data_list)  # make into a single line
            #print values
            length = len(values.split(','))
            #print length
            if length != order:
                item_record = Metrics.query.filter_by(type=types).first()
                item_record.data = values
                item_record.order = order
                db_session.commit()
                return jsonify({'message': 'SUCCESS'})
            else:

                return jsonify({'message': 'ERROR'})
        else:
            return jsonify({'message': 'ERROR'})
    item_records = Metrics.query.all()
    last_updated = StockTimestamp.query.filter_by(
        name="metrics_uploaded").first()
    return render_template(
        'admin/upload_metrics.html',
        title="upload_metrics",
        item_records=item_records,
        last_updated=last_updated)


def create_or_update_company(security):
    company_count = StockCompanies.query.filter_by(
        symbol=security['StockCode']).count()
    #print "company count == ", company_count
    if company_count >= 1:
        company = StockCompanies.query.filter_by(
            symbol=security['StockCode']).one()
        company.name = security['StockName']
        company.category = security['GICSector']
        company.asset_class = security['AssetClass']
        company.is_traded = bool(int(security['Traded']))
        db_session.commit()
        #print "company updated here"
    else:
        company = StockCompanies(name=security['StockName'],
                                 category=security['GICSector'],
                                 asset_class=security['AssetClass'],
                                 symbol=security['StockCode'],
                                 is_traded=bool(int(security['Traded'])))
        #company.name = security['StockName']
        #company.category = security['GICSector']
        #company.asset_class = security['AssetClass']
        #company.is_traded = bool(int(security['Traded']))
        db_session.add(company)
        db_session.commit()
        #print "new company added here"
    #print "company id ==", company.id
    return company


@blueprint.route('/stock_companies')
@login_required
def stock_companies():
    item_records = StockCompanies.query.all()
    # print "total companies == ",item_records.count()
    return render_template(
        'admin/stock_companies.html',
        title="stock companies",
        item_records=item_records)


@blueprint.route('/advisors')
@login_required
def advisors():
    user_records = User.query.filter(
        User.id == UserProfile.user_id,
        User.is_superuser == False,
        UserProfile.user_type == 'agent',
    ).order_by(
        desc(
            User.id))
    print(user_records.count())
    return render_template(
        'admin/advisors.html',
        user_records=user_records,
        title="Agents")


@blueprint.route('/accountants')
@login_required
def accountants():
    user_records = User.query.filter(
        User.id == UserProfile.user_id,
        User.is_superuser == False,
        UserProfile.user_type == 'accountant',
    ).order_by(
        desc(
            User.id))
    print(user_records.count())
    return render_template(
        'admin/accountants.html',
        user_records=user_records,
        title="Practising Accountants")


@blueprint.route('/cleints')
@login_required
def clients():
    client_portfolios = UserClients.query.all()
    print(len(client_portfolios))
    total=len(client_portfolios)
    return render_template(
        'admin/clients.html',
        user_records=client_portfolios, total=total,
        title="Clients")


@blueprint.route('/trustees')
@login_required
def trustees():
    user_records = User.query.filter(
        User.id == UserProfile.user_id,
        User.is_superuser == False,
        UserProfile.user_type == 'trustee',
    ).order_by(
        desc(
            User.id))
    #print user_records.count()
    return render_template(
        'admin/trustees.html',
        user_records=user_records,
        title="SMSF Trustee")


@blueprint.route('/accountClients')
@login_required
def accountClients():
    user_records = User.query.filter(
        User.id == UserProfile.user_id,
        User.is_superuser == False,
        UserProfile.user_type == 'accountclient',
    ).order_by(
        desc(
            User.id))
    #print user_records.count()
    return render_template(
        'admin/accountClients.html',
        user_records=user_records,
        title="Clients of Accountant")


@blueprint.route('/investors')
@login_required
def investors():
    user_records = User.query.filter(
        User.id == UserProfile.user_id,
        User.is_superuser == False,
        UserProfile.user_type == 'investor',
    ).order_by(
        desc(
            User.id))
    #print user_records.count()
    return render_template(
        'admin/investors.html',
        user_records=user_records,
        title="Independant Investors")


@blueprint.route('/dashboard')
@login_required
def dashboard():
    portfolios_count = 0
    holdings_count = 0
    users_count = User.query.filter_by(is_superuser=False).count()
    user_records = User.query.filter_by(
        is_superuser=False).order_by(
        desc(
            User.id)).limit(10)
    client_records = UserClients.query.order_by(desc(UserClients.id)).limit(10)
    return render_template(
        'admin/index2.html',
        user_records=user_records,
        users_count=users_count,
        portfolio_records="",
        client_records=client_records,
        portfolios_count=portfolios_count,
        holdings_count=holdings_count)


@blueprint.route('/tableCRUD', methods=['POST'])
@login_required
def all_table_crud():
    if request.method == 'POST':
        dict = request.form
        for key in dict.keys():
             for value in dict.getlist(key):
                print(key, ":", value)
        #print request.form['crud_type']
        if request.form['crud_type'] == "updatepage":
            if request.form.get(
                    'table_name') == "pages" and request.form.get('id') != "":
                pages = Pages.query.get(request.form.get('id'))
                pages.page_text = request.form.get('page_text')
                pages.page_name = request.form.get('page_name')
                pages.page_keywords = request.form.get('page_keywords')
                pages.page_description_brief = request.form.get(
                    'page_description_brief')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updateabout":
            if request.form.get(
                    'table_name') == "pages" and request.form.get('id') != "":
                pages = Pages.query.get(request.form.get('id'))
                pages.page_text = request.form.get('page_text')
                pages.page_name = request.form.get('page_name')
                pages.page_keywords = request.form.get('page_keywords')
                pages.page_description_brief = request.form.get(
                    'page_description_brief')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updategetstarted":
            if request.form.get(
                    'table_name') == "pages" and request.form.get('id') != "":
                pages = Pages.query.get(request.form.get('id'))
                pages.page_text = request.form.get('page_text')
                pages.page_name = request.form.get('page_name')
                pages.page_keywords = request.form.get('page_keywords')
                pages.page_description_brief = request.form.get(
                    'page_description_brief')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "services_iriskonline":
            if request.form.get('slug') == "services_iriskonline":
                pages = PageBlocks.query.get(request.form.get('id'))
                pages.block_desc = request.form.get('block_desc')
                pages.block_title = request.form.get('block_title')
                pages.block_short_desc = request.form.get('block_short_desc')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "services_iriskaware":
            if request.form.get('slug') == "services_iriskaware":
                pages = PageBlocks.query.get(request.form.get('id'))
                pages.block_desc = request.form.get('block_desc')
                pages.block_title = request.form.get('block_title')
                pages.block_short_desc = request.form.get('block_short_desc')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "services_iriskwatch":
            if request.form.get('slug') == "services_iriskwatch":
                pages = PageBlocks.query.get(request.form.get('id'))
                pages.block_desc = request.form.get('block_desc')
                pages.block_title = request.form.get('block_title')
                pages.block_short_desc = request.form.get('block_short_desc')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updateblock":
            if request.form.get('block_name') == "header" and request.form.get(
                    'page_id') != "":
                pageblocks = PageBlocks.query.get(request.form.get('id'))
                pageblocks.block_short_desc = request.form.get(
                    'block_short_desc')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
            if request.form.get(
                    'slug') == "footer" and request.form.get('id') != "":
                pageblocks = PageBlocks.query.get(request.form.get('id'))
                pageblocks.block_short_desc = request.form.get(
                    'block_short_desc')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updateiriskaware":
            if request.form.get(
                    'block_name') == "iriskaware_report" and request.form.get('id') != "":
                pageblocks = PageBlocks.query.get(request.form.get('id'))
                pageblocks.block_desc = request.form.get('block_desc')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updatefaqs":
            if request.form.get(
                    'table_name') == "faqs" and request.form.get('faq_id') != "":
                count_record = Faqs.query.filter_by(
                    id=request.form.get('faq_id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:

                    faq_status = True
                    if request.args.get('faq_status')=='True':
                        faq_status = False
                    item = Faqs.query.get(request.form.get('faq_id'))
                    item.faq_question = request.form.get('faq_question')
                    item.faq_answer = request.form.get('faq_answer')
                    item.faq_status = faq_status
                    #pages.page_description_brief = request.form.get('page_description_brief')
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updatetutor":
            if request.form.get('table_name') == "tutor" and request.form.get('faq_id') != "":
                count_record = TutorBlocks.query.get(request.form.get('faq_id'))
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    item = TutorBlocks.query.get(request.form.get('faq_id'))
                    item.tutor_title = request.form.get('faq_question')
                    item.tutor_text = request.form.get('faq_answer')
                    item.tutor_status = request.form.get('faq_status')
                    item.updated_on=dti.datetime.now()
                    #pages.page_description_brief = request.form.get('page_description_brief')
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "deletefaqs":
            if request.form.get(
                    'table_name') == "faqs" and request.form.get('faq_id') != "":
                count_record = Faqs.query.filter_by(
                    id=request.form.get('faq_id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    Faqs.query.filter_by(id=request.form.get('faq_id')).delete()
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "deleteblogs":
            if request.form.get(
                    'table_name') == "blogs" and request.form.get('blog_id') != "":
                count_record = SiteBlogs.query.filter_by(id=request.form.get('blog_id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    SiteBlogs.query.filter_by(id=request.form.get('blog_id')).delete()
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "deletetutor":
            if request.form.get(
                    'table_name') == "tutor" and request.form.get('faq_id') != "":
                count_record = TutorBlocks.query.filter_by(
                    id=request.form.get('faq_id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    TutorBlocks.query.filter_by(
                        id=request.form.get('faq_id')).delete()
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "deletesitemedia":
            if request.form.get('table_name') == "sitemedia" and request.form.get(
                    'media_id') != "":
                count_record = SiteMedia.query.filter_by(
                    id=request.form.get('media_id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    SiteMedia.query.filter_by(
                        id=request.form.get('media_id')).delete()
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updateblogs":
            if request.form.get('table_name') == "blogs" and request.form.get(
                    'blog_id') != "":
                count_record = SiteBlogs.query.filter_by(
                    id=request.form.get('blog_id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    item = SiteBlogs.query.get(request.form.get('blog_id'))
                    item.title = request.form.get('title')
                    item.description = request.form.get('description')
                    item.content = request.form.get('content')
                    # item.in_sitemap = request.form.get('in_sitemap')
                    item.status = request.form.get('in_sitemap')
                    # pages.page_description_brief = request.form.get('page_description_brief')
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "updatess":
            if request.form.get('table_name') == "software_settings" and request.form.get(
                    'settings_slug') != "":
                ssettings = SoftwareSettingss.query.filter_by(
                    settings_slug=request.form.get('settings_slug')).one()
                ssettings.free_slots = request.form.get('free_slots')
                ssettings.idle_timeout = request.form.get('idle_timeout')
                ssettings.minimum_postcode = request.form.get('minimum_postcode')
                #ssettings.maximum_postcode = request.form.get('maximum_postcode')
                #ssettings.iriskware_upload = request.form.get('iriskware_upload')
                ssettings.paypal_email = request.form.get('paypal_email')
                ssettings.site_address = request.form.get('site_address')
                ssettings.site_city = request.form.get('site_city')
                ssettings.site_state = request.form.get('site_state')
                ssettings.site_zipcode = request.form.get('site_zipcode')
                ssettings.site_email = request.form.get('site_email')
                ssettings.site_phone = request.form.get('site_phone')
                ssettings.idle_timeout = request.form.get('idle_timeout')
                ssettings.cooltrader_username = request.form.get('cooltrader_username')
                ssettings.cooltrader_password = request.form.get('cooltrader_password')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})

        else:
            return jsonify({'message': 'ERROR'})

        print(type(result))
        return result
    else:
        return jsonify({'message': 'Unsupported Request'})


@blueprint.route('/login', methods=['GET', 'POST'])
def adminLogin():

    error = None
    login_form = LoginForm(csrf_enabled=True)
    if request.method == 'POST':
        print(request.form['email'])
        emailid, password = request.form['email'].lower(
        ), request.form['password']
        #emailid = emailid.encode('ascii', 'ignore')
        #password = password.encode('ascii', 'ignore')
        try:
            user = User.query.filter_by(email=str(emailid), is_superuser=True).first()
            #print(user.id)
        except NoResultFound:
            error = "No user with that email address exists."
        else:
            if user:
                remember_me = False
                if 'remember_me' in request.form:
                    remember_me = True
                if user.password:
                    if django_pbkdf2_sha256.verify(
                            str(password), user.password):
                        session['user_id'] = user.id
                        login_user(user, remember=remember_me)
                        return redirect_next("/admin/dashboard")
                    error = "Incorrect password."
                else:
                    error = "You have not set up a password yet. Use the 'Forgot Password' link below to reset it."
            else:
                error = "No user with that data exists"
    else:
        user = current_user
        user.authenticated = False
        logout_user()
        session.clear()
        logout_user()
    return render_template(
        'admin/login.html',
        error=error,
        login_form=login_form)

# Model view for edit


@blueprint.route('/editpricingModalView', methods=['POST'])
@login_required
def edit_table_view_form():
    if request.method == 'POST':
        form_title = "Edit Pricing Table"
        #print request.form['table_name']
        #print request.form['id']
        if request.form['table_name'] == "pricing":
            if request.form.get('id'):
                result_items = PricingTable.query.filter_by(
                    id=request.form.get('id')).first()
            else:
                result_items = PricingTable.query.all()
            form_title = "Edit Pricing Table"
            table_name = request.form['table_name']
        else:
            return jsonify({'message': 'ERROR'})
        # print result_items.id
        # print result_items.name
        # print result_items.description
        return render_template(
            'admin/edit_pricing_modal.html',
            item_details=result_items,
            title='Pricing Table',
            form_title=form_title,
            table_name=table_name)
    else:
        return jsonify({'message': 'Unsupported Request'})


@blueprint.route('/tableEdit', methods=['POST'])
@login_required
def edit_table_details():
    if request.method == 'POST':
        #print request.form['table_name']
        #print request.form['id']
        #id = request.form['id']
        if request.form['table_name'] == "pricing_table":
            if request.form.get('id') != "":
                count_record = PricingTable.query.filter_by(
                    id=request.form.get('id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    update_record = PricingTable.query.get(
                        request.form.get('id'))
                    update_record.name = request.form.get('name')
                    update_record.description = request.form.get('description')
                    update_record.monthly = request.form.get('monthly')
                    update_record.yearly = request.form.get('yearly')
                    update_record.quarterly = request.form.get('quarterly')
                    update_record.status = request.form.get('status')
                    update_record.pricing_type = request.form.get(
                        'pricing_type')
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['table_name'] == "content_emails":
            if request.form.get('id') != "":
                count_record = Emails.query.filter_by(
                    id=request.form.get('id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    update_record = Emails.query.get(request.form.get('id'))
                    update_record.email_subject = request.form.get('email_subject')
                    update_record.email_message = request.form.get('email_message')
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['table_name'] == "userprofile":
            if request.form.get('id') != "":
                count_record = UserProfile.query.filter_by(
                    id=request.form.get('id')).first()
                if count_record is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    #print request.form.get('statusvalue')
                    if request.form.get('statusvalue') == "True":
                        status = False
                    else:
                        status = True
                    try:
                        update_record = UserProfile.query.get(
                            request.form.get('id'))
                        update_record.afsl_activate = status
                        db_session.commit()
                        result = jsonify({'message': 'SUCCESS'})
                    except BaseException:
                        db_session.rollback()
                        result = jsonify({'message': 'ERROR'})

        else:
            return jsonify({'message': 'ERROR'})
        print(type(result))
        return result
    else:
        return jsonify({'message': 'Unsupported Request'})

# Model view for edit


@blueprint.route('/editcouponModalView', methods=['POST'])
@login_required
def edit_coupon_view_form():
    if request.method == 'POST':
        form_title = "Edit Coupon Table"
        #print request.form['table_name']
        #print request.form['id']
        if request.form['table_name'] == "coupons":
            if request.form.get('id'):
                result_items = CouponCodes.query.filter_by(
                    advisor_id=request.form.get('id')).first()
                if result_items:

                    return jsonify({'message': 'Already Created'})
                else:
                    result_items = {'id': '', 'advisor_id': request.form['id']}
            else:
                #result_items = CouponCodes.query.all()
                result_items = {'id': '', 'advisor_id': request.form['id']}
            form_title = "Create Coupon"
            table_name = request.form['table_name']
            code = str(uuid.uuid4().fields[-1])[:9]
        else:
            return jsonify({'message': 'ERROR'})
        # print result_items.id
        return render_template(
            'admin/edit_coupon_modal.html',
            item_details=result_items,
            code=code,
            title='Coupon Table',
            form_title=form_title,
            table_name=table_name)
    else:
        return jsonify({'message': 'Unsupported Request'})

# Model view for edit



@blueprint.route('/TutorEditModalView', methods=['POST'])
@login_required
def edit_tutor_view_form():
    if request.method == 'POST':
        form_title = "Edit Tutor Item"
        if request.form['table_name'] == "tutor":
            if request.form.get('id'):
                result_items = TutorBlocks.query.filter_by(id=request.form.get('id')).first()
                if result_items is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    result_items = result_items
            else:
                return jsonify({'message': 'ERROR'})
            table_name = request.form['table_name']
        else:
            return jsonify({'message': 'ERROR'})
        return render_template(
            'admin/edit_tutor_modal.html',
            item_details=result_items,
            title='Faq Table',
            form_title=form_title,
            table_name=table_name)
    else:
        return jsonify({'message': 'Unsupported Request'})
# Model view for edit


@blueprint.route('/blogEditModalView', methods=['POST'])
@login_required
def edit_blog_view_form():
    if request.method == 'POST':
        form_title = "Edit Blog Table"
        # print request.form['table_name']
        # print request.form['id']
        if request.form['table_name'] == "blogs":
            if request.form.get('id'):
                result_items = SiteBlogs.query.filter_by(
                    id=request.form.get('id')).first()
                if result_items is None:
                    return jsonify({'message': 'ERROR'})
                else:
                    result_items = result_items
            else:
                return jsonify({'message': 'ERROR'})
            form_title = "Edit Blog"
            table_name = request.form['table_name']
        else:
            return jsonify({'message': 'ERROR'})
        # print result_items.id
        return render_template(
            'admin/edit_blog_modal.html',
            item_details=result_items,
            title='Blog Table',
            form_title=form_title,
            table_name=table_name)
    else:
        return jsonify({'message': 'Unsupported Request'})


@blueprint.route('/tableCreate', methods=['POST'])
@login_required
def create_table_details():
    if request.method == 'POST':
        dict = request.form
        for key in dict.keys():
            for value in dict.getlist(key):
                print(key, ":", value)
        #print request.form['table_name']
        if request.form['table_name'] == "register":
            if request.form.get('email') == "":
                return jsonify({'message': 'Email is required'})
            if request.form.get('email') != "":
                check_email = User.query.filter_by(
                    email=request.form.get('email')).first()
            if check_email:
                return jsonify({'message': 'Email Already Exisits'})
            else:
                try:
                    email = request.form.get('email')
                    # hashes = sha256(str(email) + str(int(time()))).hexdigest()

                    password = hlp.randomString(11)
                    password_hash = django_pbkdf2_sha256.encrypt(password)
                    print(password)
                    result_items = User(
                        username=email,
                        email=email,
                        first_name=request.form.get('first_name'),
                        last_name=request.form.get('last_name'),
                        password=password_hash,
                        last_login=dttim.datetime.now(),
                        is_superuser=False,
                        is_staff=False,
                        is_active=True,
                        date_joined=dttim.datetime.now())
                    db_session.add(result_items)
                    db_session.commit()
                    if result_items:
                        advisor_id = None
                        print (result_items.id)
                        hashes = sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()
                        userprofile_items = UserProfile(
                            user_id=result_items.id,
                            phone_number=request.form.get('phone'),
                            zip_code=request.form.get('zip_code'),
                            city=request.form.get('city'),
                            state=request.form.get('state'),
                            home_address=request.form.get('home_address'),
                            alternate_phone=request.form.get('alternate_phone'),
                            postcode=request.form.get('zip_code'),
                            activation_code=hashes,
                            user_type=request.form.get('user_type'),
                            advisor_id=advisor_id,
                            license_number=request.form.get('license_number'),
                            license_name=request.form.get('license_name'),
                            license_date=request.form.get('license_date'),
                            license_state=request.form.get('license_state'),
                            # afsl=request.form.get('afsl'),
                            afsl_activate=False)
                        db_session.add(userprofile_items)
                        db_session.commit()
                        emails.user_manual_register_email(result_items,password, hashes)
                        insert_into = AdvisorSubscriptions(
                            paypal_return_data='manual',
                            paypa_txn_id=email,
                            transaction_status='Success',
                            user_uuid=int(hashlib.md5(email.encode('utf-8')).hexdigest(), 16),
                            amount=0,
                            subscription_date=dttim.datetime.now(),
                            paypal_verify_sign='Manual',
                            subscription_code=email)
                        db_session.add(insert_into)
                        db_session.commit()
                        result = jsonify({'message': 'SUCCESS'})
                except Exception as e:
                    return jsonify({'message': 'ERROR'})
                    print(e)
        elif request.form['table_name'] == "faqs":
            if request.form.get('name') != "":
                count_record = Faqs.query.filter_by(
                    faq_question=request.form.get('name')).first()
                if count_record:
                    return jsonify({'message': 'ERROR'})
                else:
                    print(request.form.get('answer'))
                    result_items = Faqs(
                        faq_question=request.form.get('name'),
                        faq_answer=request.form.get('answer'))
                    db_session.add(result_items)
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['table_name'] == "tutor":
            if request.form.get('name') != "":
                count_record = TutorBlocks.query.filter_by(
                    tutor_title=request.form.get('name')).first()
                if count_record:
                    return jsonify({'message': 'ERROR'})
                else:
                    code = str(uuid.uuid4().fields[-1])[:9]
                    result_items = TutorBlocks(
                        tutor_title=request.form.get('name'),
                        tutor_text=request.form.get('description'),
                        tutor_reference_code=code,
                        created_on=dti.datetime.now(),
                        updated_on=dti.datetime.now())
                    db_session.add(result_items)
                    db_session.commit()
                    result = jsonify({'message': 'SUCCESS'})
        elif request.form['table_name'] == "blogs":
            if request.form.get('title') != "":
                count_record = SiteBlogs.query.filter_by(
                    title=request.form.get('title')).first()
                if count_record:
                    return jsonify({'message': 'ERROR'})
                else:
                    result_items = SiteBlogs(
                        title=request.form.get('title'),
                        status=2,
                        user_id=current_user.id,
                        description=request.form.get('description'),
                        rating_sum=0,
                        created=datetime.now(),
                        updated=datetime.now(),
                        rating_count=0,
                        comments_count=0,
                        site_id=1,
                        rating_average=1,
                        keywords_string=request.form.get('description'),
                        content=request.form.get('content'))

                    try:
                        db_session.add(result_items)
                        db_session.commit()
                        result = jsonify({'message': 'SUCCESS'})
                    except BaseException:
                        db_session.rollback()
                        print("Unexpected error:", sys.exc_info()[0])
                        result = jsonify({'message': 'ERROR'})


        else:
            return jsonify({'message': 'ERROR'})
        print(type(result))
        return result
    else:
        return jsonify({'message': 'Unsupported Request'})


@blueprint.route('/logout')
def logout():
    """Logout the current user."""
    try:
        user = current_user
        user.authenticated = False
        logout_user()
        session.clear()
        logout_user()
    except Exception as e:
        print(e)
    return redirect_next("/admin/login")
