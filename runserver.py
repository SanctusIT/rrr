import sys
from flask import render_template
from database import db_session
import emails
from filters import human_date, human_time, pricing_detail_name, percentage, currency, round_the_value, \
    site_settings, portfolio_detail_name
from helpers import yes_no, encode_key, decode_key
from views.models import PageBlocks, Pages
#from admin.models import SoftwareSettingss
#from admin.models import SoftwareSettingss
import datetime as dtti
import settings
import views
import admin
import extensions
import flask_login
from flask import Flask, request
from flask import session as sess
import filters as fltrs

def create_app(config_object="prod"):
    app = Flask(__name__)
    app.config.from_object(settings)
    extensions.login_manager.init_app(app)
    extensions.login_manager.session_protection = 'strong'
    emails.mail.init_app(app)
    register_blueprints(app)
    wrap_teardown_funcs(app)
    register_errorhandlers(app)
    #login_manager.login_view = 'login'
    # login_manager.user_loader(load_user)
    app.template_filter('pricing_detail_name')(pricing_detail_name)
    app.template_filter('portfolio_detail_name')(portfolio_detail_name)
    app.template_filter('human_date')(human_date)
    app.template_filter('human_time')(human_time)
    app.template_filter('currency')(currency)
    app.template_filter('percentage')(percentage)
    app.template_filter('round_the_value')(round_the_value)
    app.template_filter('site_settings')(site_settings)
    app.template_filter('yes_no')(yes_no)
    app.template_filter('encode_key')(encode_key)
    app.template_filter('decode_key')(decode_key)
    app.template_filter('count_cart_items')(fltrs.count_cart_items)
    return app

def register_blueprints(app):
    app.register_blueprint(views.simple.blueprint)
    #app.register_blueprint(views.iriskonline.blueprint)
    #app.register_blueprint(views.payments.blueprint)
    app.register_blueprint(admin.admin.blueprint)
    return None

def wrap_teardown_funcs(app):
    def wrap_teardown_func(teardown_func):
        def log_teardown_error(*args, **kwargs):
            try:
                teardown_func(*args, **kwargs)
            except Exception as exc:
                app.logger.exception(exc)

        return log_teardown_error

    if app.teardown_request_funcs:
        for bp, func_list in app.teardown_request_funcs.items():
            for i, func in enumerate(func_list):
                app.teardown_request_funcs[bp][i] = wrap_teardown_func(func)
    if app.teardown_appcontext_funcs:
        for i, func in enumerate(app.teardown_appcontext_funcs):
            app.teardown_appcontext_funcs[i] = wrap_teardown_func(func)

def register_errorhandlers(app):
    def render_error(error):
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)

        #emails.error_email()
        if error_code in [401, 404, 500]:
            return render_template("{0}.html".format(403)), 403
        else:
            return render_template("{0}.html".format(error_code)), error_code

    # for errcode in [401, 404, 500]:
    app.errorhandler(401)(render_error)
    return None

app = create_app("prod")

@app.errorhandler(403)
def error_403(e):
    #emails.error_email(403)
    return render_template('403.html'), 403


@app.errorhandler(404)
def error_404(e):
    #emails.error_email(404)
    return render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    #emails.error_email(500)
    return render_template('500.html'), 500


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


#@app.before_request
def my_before_request():
    #site_settings = SoftwareSettingss.query.first()
    #sess['site_settings'] = site_settings
    #print sess['site_settings']
    print("*" * 40)

    '''
    if session['home_page_header'] != None:
        print session['home_page_header']
        print "*"*40
        session['home_page_header'] = session['home_page_header']
    else:
        home_page_header = PageBlocks.query.filter_by(block_slug="header").first()
        session['home_page_header'] = home_page_header
        print session['home_page_header']
        print "*"*40
    print session['home_page_header']
    print "*"*40

    print "*"*40
    home_page_header = PageBlocks.query.filter_by(block_slug="header").first()
    session['home_page_header'] = home_page_header
    print session['home_page_header']
    systemsettings = admin.models.SoftwareSettingss.query.first()
    session['systemsettings'] = systemsettings
    print "*"*40
    '''

#@app.after_request
def my_after_request(response):
    print("&" * 40)
    #site_settings = SoftwareSettingss.query.first()
    #sess['site_settings'] = site_settings
    #print sess['site_settings']

@app.context_processor
def page_blocks():
    def static_content(slug, currency=u'$'):
        query = PageBlocks.query.filter_by(block_slug=slug).first()
        return query.block_short_desc
    return dict(static_content=static_content)



if __name__ == '__main__':
    port = None
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(host='0.0.0.0', debug=True, port=port)
