from datetime import datetime

from flask import redirect, request, session

import constants
import views.models as vm
import base64
from settings import SECRET_KEY
from hashids import Hashids
import random
import string

def redirect_next(default='/'):
    location = request.args['next'] if request.args.get('next') else ''
    return redirect(location or default)


def date_from_form_field(field):
    if not field:
        return None
    return datetime.strptime(field, '%m/%d/%Y').date()


def time_from_form_field(field):
    if not field:
        return None
    return datetime.strptime(field, '%I:%M %p').time()


def yes_no(val):
    if val:
        return 'Yes'
    return 'No'


def get_current_user():
    if session.get('user_id'):
        return vm.User.query.get(session['user_id'])
    return None

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def has_permission(section, action):
    this_user = get_current_user()
    if this_user is None:
        return False
    allow = False
    if this_user.person.is_admin or this_user.person.type == 'program-manager':
        allow = True
    else:
        permissions = this_user.person.entity.settings.get('permissions', {})
        role_types = set(r.type for r in this_user.person.roles)
        for role in role_types:
            if (
                permissions and action in permissions.get(
                    role,
                    {}).get(
                    section,
                    [])) or action in constants.DEFAULT_PERMISSIONS.get(
                    role,
                    {}).get(
                        section,
                    []):
                allow = True
    return allow

def encode_key(id):
    hashids = Hashids(salt=SECRET_KEY, min_length=24)
    return hashids.encode(id) # to encode

def encode_key1(string):
    key = SECRET_KEY
    encoded_chars = []
    for i in xrange(len(string)):
        key_c = key[i % len(key)]
        encoded_c = chr(ord(string[i]) + ord(key_c) % 256)
        encoded_chars.append(encoded_c)
    encoded_string = "".join(encoded_chars)
    return base64.urlsafe_b64encode(encoded_string)

def decode_key(hashed):
    # print type(hashed)
    hashids = Hashids(salt=SECRET_KEY, min_length=24)
    decoded_id = hashids.decode(hashed)
    # print decoded_id
    return  decoded_id # to decode

def decode_key2(string):
    key = SECRET_KEY
    encoded_chars = []
    for i in xrange(len(string)):
        key_c = key[i % len(key)]
        encoded_c = chr(ord(string[i]) - ord(key_c) % 256)
        encoded_chars.append(encoded_c)
    encoded_string = "".join(encoded_chars)
    return base64.urlsafe_b64encode(encoded_string)