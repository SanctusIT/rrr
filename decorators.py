from functools import wraps

from flask import abort, redirect, request, session, url_for

from helpers import get_current_user, has_permission


def require_logged_in(f):
    @wraps(f)
    def validator(*args, **kwargs):
        if session.get('user_id'):
            return f(*args, **kwargs)
        if request.path in ['', '/']:
            next = None
        else:
            next = request.path
        return redirect(url_for('simple.login', next=next))
    return validator


def require_superadmin(f):
    @wraps(f)
    def validator(*args, **kwargs):
        this_user = get_current_user()
        if this_user.person.type == 'superadmin':
            return f(*args, **kwargs)
        abort(403)
    return require_logged_in(validator)


def require_admin(f):
    @wraps(f)
    def validator(*args, **kwargs):
        this_user = get_current_user()
        if this_user.person.is_admin:
            return f(*args, **kwargs)
        abort(403)
    return require_logged_in(validator)


def require_advisor(f):
    @wraps(f)
    def validator(*args, **kwargs):
        this_user = get_current_user()
        if this_user.userprofile.user_typetype == 'advisor':
            return f(*args, **kwargs)
        abort(403)
    return require_logged_in(validator)


def require_permission(section, action, check_session=True):
    def decorator(f):
        @wraps(f)
        def validator(*args, **kwargs):
            if check_session and not session.get('entity_id'):
                return redirect(url_for('simple.home'))
            if has_permission(section, action):
                return f(*args, **kwargs)
            abort(403)
        return require_logged_in(validator)
    return decorator
