# Django settings for mw project.

import os
import django

DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__file__))
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG


ADMINS = (
    #('Araknos support', 'support@araknos.it'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS = [
    "*"
]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
# TIME_ZONE = ''
# USE_TZ = True  # Uncomment to use timezone support
TIME_ZONE = None

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    #os.path.join(os.environ["AKAB_CODE_ROOT"], "ui"),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '1l=!s0@!_c&%rz6+#vaye-y25w-^am!@2lwh1%cr+cz0k+9)x1'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'middleware.AkMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'mw.urls'

#AUTH_USER_MODEL = 'aaa.AkabUser'
AUTH_USER_MODEL = 'configs.User'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # 'django.contrib.sites',
    # 'django.contrib.messages',
    #'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'django_extensions',  # development tool
    'south',

)

# FIXTURE_DIRS = (
#     #os.path.join(SITE_ROOT, 'aaa/fixtures'),
#     os.path.join(SITE_ROOT, 'ui/fixtures'),
# )

CONF = conf = {}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'akab': {
            'format': "[%(name)s:%(lineno)s] %(message)s"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        # 'logfile': {
        #     'level':'DEBUG',
        #     'class':'logging.handlers.RotatingFileHandler',
        #     'filename': LOGFILE_PATH,
        #     'maxBytes': 50000,
        #     'backupCount': 2,
        #     'formatter': 'standard',
        # },
        'console_for_tests': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'console': {
            # 'level':'DEBUG',
            'level': 'CRITICAL',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'akab_syslog': {
            'level': 'DEBUG',
            'class': 'akLogging.AkSyslogHandler',
            'app_name': 'mw',
            'formatter': 'akab'
        },
    },
    'loggers': {
        'django': {
            #'handlers':['console'],
            'handlers': ['console', 'akab_syslog'],
            'propagate': True,
            'level': 'DEBUG',
            # 'level': 'WARNING',
        },
        'django.db.backends': {
            'handlers': ['console'],
            #'level': 'DEBUG',
            'level': 'WARNING',
            'propagate': False,
        },
        'aaa': {
            #'handlers': ['console'],
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'action': {
            #'handlers': ['console'],
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'akbase': {
            #'handlers': ['console'],
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'configs': {
            #'handlers': ['console'],
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'catalog': {
            #'handlers': ['console'],
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'system': {
            #'handlers': ['console'],
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'query': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'real_time': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'ui': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'generic_data_storage': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'categories': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'report_manager': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'notification': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'control': {
            'handlers': ['console', 'akab_syslog'],
            'level': 'DEBUG',
        },
        'tests': {
            'handlers': ['console_for_tests'],
            'level': 'INFO',
        },
    }
}

MAX_CONNECTED_USERS = 10
BL_POST_RETRY_TIMES = 240
BL_POST_RETRY_INTERVAL_SECONDS = 0.5
RESERVED_USERNAMES = ("admin", "AKdebug")
DEFAULT_PASSWORD_MAX_AGING = 90  # Days

# MW execution mode ("DEV", "PROD")
# DEV for local development out of Akab2 env
# PROD for production use
CONF["MODE"] = "PROD"

# UI execution mode ("DEV", "PROD")
# DEV for local development out of Akab2 env
# PROD for production use
CONF["UIMODE"] = "DEV"
#conf["BRAND"] = "default"

if CONF["MODE"] != "DEV":

    DATABASES = {
        'default': {
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or
            # 'oracle'.
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            #'ENGINE': 'django.db.backends.sqlite3',
            # Database name
            'NAME': 'mw',
            'USER': 'akab',                  # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            # Set to empty string for localhost. Not used with sqlite3.
            'HOST': '127.0.0.1',
            # Set to empty string for default. Not used with sqlite3.
            'PORT': '5432',
        },
        "akab2": {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'HOST': 'localhost',
            'NAME': 'akab2',
            'PASSWORD': None,
            'PORT': 5432,
            'USER': 'akab',
            'OPTIONS': {"async": False}
        }

    }

else:
    DATABASES = {
        'default': {
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or
            # 'oracle'.
            'ENGINE': 'django.db.backends.sqlite3',
            # Database name
            'NAME': os.path.join(SITE_ROOT, 'mw.sql'),
            'USER': 'akab',                  # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            # Set to empty string for localhost. Not used with sqlite3.
            'HOST': '',
            # Set to empty string for default. Not used with sqlite3.
            'PORT': '',
        }
    }

try:
    from local_settings import *
except ImportError:
    pass

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT, 'templates'),
)
if CONF["MODE"] != 'DEV':
    TEMPLATE_DIRS = (
        # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
        # Always use forward slashes, even on Windows.
        # Don't forget to use absolute paths, not relative paths.
        os.path.join(STATIC_URL, "ui"),
    )


# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''
if CONF["MODE"] != 'DEV':
    STATIC_ROOT = os.path.join(STATIC_URL, "mw")
