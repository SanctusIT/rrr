from datetime import date, timedelta

import sqlalchemy
from sqlalchemy.engine import reflection
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.schema import DropConstraint, DropTable, ForeignKeyConstraint, MetaData, Table

import settings

engine = sqlalchemy.create_engine(
    'postgresql+psycopg2://%s:%s@%s/%s' %
    (settings.DB['user'],
     settings.DB['password'],
     settings.DB['host'],
     settings.DB['database']),
    echo=settings.DEBUG,
    pool_recycle=7200,
    pool_size=10)
try:
    db_session = scoped_session(sessionmaker(bind=engine))
# may need more exceptions here (or trap all)
except sqlalchemy.exc.OperationalError:
    db_session.rollback()
    engine = sqlalchemy.create_engine(
        'postgresql+psycopg2://%s:%s@%s/%s' %
        (settings.DB['user'],
         settings.DB['password'],
         settings.DB['host'],
         settings.DB['database']),
        echo=settings.DEBUG,
        pool_recycle=7200,
        pool_size=10)
    db_session = scoped_session(sessionmaker(
        bind=engine))  # replace your connection

Base = declarative_base()
Base.query = db_session.query_property()


# Stolen from: http://www.sqlalchemy.org/trac/wiki/UsageRecipes/DropEverything
def drop_everything():
    inspector = reflection.Inspector.from_engine(engine)
    metadata = MetaData()

    tbs = []
    all_fks = []

    for table_name in inspector.get_table_names():
        fks = []
        for fk in inspector.get_foreign_keys(table_name):
            if not fk['name']:
                continue
            fks.append(
                ForeignKeyConstraint((), (), name=fk['name'])
            )
        t = Table(table_name, metadata, *fks)
        tbs.append(t)
        all_fks.extend(fks)

    for fkc in all_fks:
        db_session.execute(DropConstraint(fkc))

    for table in tbs:
        db_session.execute(DropTable(table))

    db_session.commit()