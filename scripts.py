import csv
from datetime import datetime

def string_to_date(date_str):
    if not date_str.strip():
        return None
    return datetime.strptime(date_str, '%m/%d/%Y')


def reformat_date(date_str):
    d = string_to_date(date_str)
    if not d:
        return ''
    return d.strftime('%Y-%m-%d')
