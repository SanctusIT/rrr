import datetime
import views.models as cn
from admin import models as adm
import locale
from fiscalyear import *

locale.setlocale(locale.LC_ALL)


def human_date(date_val, default=''):
    """
    Produces a human-readable date from a date string or date object.
    """

    if not date_val:
        return default
    if not isinstance(date_val, (datetime.date, datetime.datetime)):
        date_val = datetime.datetime.strptime(date_val, '%Y-%m-%d').date()
    return date_val.strftime('%m/%d/%Y')


def human_time(time_val, default=''):
    """
    Produces a human-readable time.
    """

    if not time_val:
        return default
    if isinstance(time_val, (datetime.datetime, datetime.time)):
        return time_val.strftime('%I:%M %p')
    return time_val

def pricing_detail_name(userid):
    result = cn.PricingTable.query.filter_by(id=userid).first()
    # print result.name
    return result.name

def site_settings(field_name):
    result = adm.SoftwareSettingss.query.filter_by(id=4).first()
    # print result.name
    value=result.site_email
    if field_name=='site_zipcode':
        value=result.site_zipcode
    if field_name=='site_state':
        value=result.site_state
    if field_name == 'site_city':
        value = result.site_city
    if field_name == 'site_address':
        value = result.site_address
    if field_name == 'site_phone':
        value = result.site_phone
    if field_name == 'paypal_email':
        value = result.paypal_email
    if field_name == 'free_slots':
        a = FiscalDate.today()
        print(a)
        print(a.quarter)
        if a.quarter==3:
            value = 75
        elif a.quarter==4:
            value=50
        elif a.quarter == 1:
            value=25
        else:
            value=result.free_slots
        print(value)
    return value

def portfolio_detail_name(pid):
    result = cn.UserPortfolios.query.filter_by(id=pid).first()
    # print result.name
    return result.name

def percentage(numerator, denominator):
    # print 'numerator', numerator
    # print 'denominator', denominator
    try:
        # print "%.2f%%" % ((float(numerator) / float(denominator)) * 100)
        return "%.2f%%" % ((float(numerator) / float(denominator)) * 100)
    except ValueError:
        return ''

def currency(value):
    return locale.currency(value, grouping=True)

def round_the_value(value):
    try:
        rounded_value = float("{0:.2f}".format(value))
    except Exception:
        rounded_value = value
    return rounded_value

def count_cart_items(uid):
    cart_items = cn.ShoppingCartItems.query.filter_by(user_id=uid, cart_item_status='NEW')
    total_items = cart_items.count()
    return total_items
