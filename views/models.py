from datetime import date, datetime, timedelta
import json

from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.ext.mutable import Mutable
from sqlalchemy.orm import backref, relationship
import sqlalchemy.types as types
from database import Base
from extensions import login_manager
from sqlalchemy.inspection import inspect
from sqlalchemy.orm import column_property
from sqlalchemy import select, func
from sqlalchemy.ext.hybrid import hybrid_method
import datetime


class Serializer(object):

    def serialize(self):
        return {c: getattr(self, c) for c in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]


def accessory_status(date_val, warning_days=60, critical_days=30):
    if not date_val or date_val > date.today() + timedelta(days=warning_days):
        return 'ok'
    if date_val > date.today() + timedelta(days=critical_days):
        return 'warning'
    if date_val > date.today():
        return 'critical'
    return 'expired'


class MutableDict(Mutable, dict):
    @classmethod
    def coerce(cls, key, value):
        """
        Convert plain dictionaries to MutableDict.
        """

        if not isinstance(value, MutableDict):
            if isinstance(value, dict):
                return MutableDict(value)
            return Mutable.coerce(key, value)
        else:
            return value

    def __setitem__(self, key, value):
        """
        Detect dictionary set events and emit change events.
        """

        dict.__setitem__(self, key, value)
        self.changed()

    def __delitem__(self, key):
        """
        Detect dictionary del events and emit change events.
        """

        dict.__delitem__(self, key)
        self.changed()


class JsonField(types.TypeDecorator):
    impl = types.Text

    def process_bind_param(self, value, engine):
        if value is None:
            return None
        return json.dumps(value)

    def process_result_value(self, value, engine):
        if value is None:
            return None
        return json.loads(value)


MutableDict.associate_with(JsonField)


def status_func(attr_name):
    def status_method(self):
        attr = getattr(self, attr_name)
        return accessory_status(
            attr,
            self.STATUS_WARNING_DAYS,
            self.STATUS_CRITICAL_DAYS)
    return status_method


class StatusMixin(object):
    status = property(status_func('expiration_date'))


'''
class SoftwareSettings(Base, StatusMixin):
    __tablename__ = 'software_settings'

    STATUS_WARNING_DAYS = 90
    STATUS_CRITICAL_DAYS = 30

    id = Column(Integer, primary_key=True)
    #entity_id = Column(Integer, ForeignKey('entities.id'))
    distributor_id = Column(Integer)
    site_name = Column(String(254), index=True, unique=True)
    site_description = Column(String(254), index=True)
    site_keywords = Column(Text, nullable=False)
    site_tag = Column(Text, default='')
    updated_date = Column(Date)
    certification_date = Column(Date)
    site_email = Column(String(100))
    site_hosting = Column(String(100))
    site_domain = Column(String(254))
    site_subdomain = Column(String(250))
    site_theme = Column(String(16))
    site_logo = Column(Text)
    site_logo_large = Column(Text)
    training_card = Column(Boolean, default=False)
    data = Column(JsonField, default={}, nullable=False)
'''


class UserProfile(Base):
    __tablename__ = 'irisk_userprofile'

    # def __repr__(self):
    #    return '<profile id=%s>' % self.id

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('auth_user.id'))
    city = Column(String(22))
    zip_code = Column(String(22))
    afsl = Column(String(22))
    user_type = Column(String(22))
    company = Column(String(52))
    postcode = Column(String(10))
    advisor_id = Column(Integer)
    accountant_id = Column(Integer)
    registration_body = Column(String(52))
    registration_number = Column(String(52))
    phone_number = Column(String(52))
    home_address = Column(String(200))
    state = Column(String(100))
    alternate_phone = Column(String(100))
    activation_code = Column(Text)
    password_reset_hash = Column(Text)
    license_number = Column(String(200))
    license_name = Column(String(200))
    license_date = Column(String(200))
    license_state = Column(String(200))
    afsl_activate = Column(Boolean, nullable=False, default=True)
    is_loggedin = Column(Boolean, nullable=False, default=False)
    activation_status = Column(Boolean, nullable=False, default=False)
    module_iriskonline = Column(Boolean, nullable=False, default=True)
    module_iriskwatch = Column(Boolean, nullable=False, default=True)
    module_iriskaware = Column(Boolean, nullable=False, default=True)


class User(Base):
    __tablename__ = 'auth_user'
    '''
    def __repr__(self):
        return '<User id=%s>' % self.id
    '''
    id = Column(Integer, primary_key=True)
    username = Column(String(54))
    password = Column(String(54))
    email = Column(String(54))
    first_name = Column(String(54))
    last_name = Column(String(54))
    is_staff = Column(Boolean, nullable=False, default=False)
    is_superuser = Column(Boolean, nullable=False, default=False)
    is_active = Column(Boolean, nullable=False, default=True)
    date_joined = Column(String(154))
    last_login = Column(String(154))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3
    #userprofile = relationship('UserProfile', lazy='dynamic')
    userid = relationship('UserProfile', uselist=False)
    #coupon_code = relationship('CouponCodes', uselist=False)

class UserClients(Base):
    __tablename__ = 'irisk_clients'

    # def __repr__(self):
    #    return '<profile id=%s>' % self.id

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('auth_user.id'))
    #email = Column(String(254))
    client_address = Column(String(254))
    client_email = Column(String(254))
    client_comments = Column(String(254))
    client_reason = Column(String(254))
    client_last_name = Column(String(254))
    client_first_name = Column(String(254))
    client_state = Column(String(254))
    client_city = Column(String(254))
    client_zipcode = Column(String(254))
    client_buy_sell = Column(String(50))
    is_active = Column(Boolean, nullable=False, default=True)
    created_on = Column(String(174))
    updated_on = Column(String(174))

class Vendor(Base):
    __tablename__ = 'vendors'

    id = Column(Integer, primary_key=True)
    entity_id = Column(Integer)
    name = Column(Text)
    type = Column(Text)
    contact = Column(Text)
    email = Column(String(254))
    phone = Column(String(16))


class Role(Base):
    __tablename__ = 'roles'

    person_id = Column(Integer, ForeignKey('personnel.id'), primary_key=True)
    site_id = Column(Integer, ForeignKey('sites.id'), primary_key=True)
    type = Column(String(24), primary_key=True)

#login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Pages(Base):
    __tablename__ = 'content_pages'

    id = Column(Integer, primary_key=True)
    page_name = Column(Text)
    page_text = Column(Text)
    page_keywords = Column(Text)
    page_description_brief = Column(Text)
    created_on = Column(String(26))
    modified_on = Column(String(26))
    page_slug = Column(String(26))


class PageBlocks(Base):
    __tablename__ = 'content_pages_blocks'

    id = Column(Integer, primary_key=True)
    page_id = Column(Integer, ForeignKey('content_pages.id'))
    block_title = Column(Text)
    block_slug = Column(Text)
    block_short_desc = Column(Text)
    block_desc = Column(Text)
    block_image_1 = Column(Text)
    block_image_2 = Column(Text)
    block_image_3 = Column(Text)
    block_image_4 = Column(Text)
    block_image_5 = Column(Text)


class Faqs(Base):
    __tablename__ = 'content_faqs'
    id = Column(Integer, primary_key=True)
    faq_question = Column(Text)
    faq_answer = Column(Text)
    faq_status = Column(Boolean, default=True)
    faq_category = Column(String(26))


class TutorBlocks(Base):
    __tablename__ = 'content_tutor'
    id = Column(Integer, primary_key=True)
    tutor_title = Column(Text)
    tutor_text = Column(Text)
    tutor_status = Column(Boolean, default=True)
    tutor_category = Column(String(26))
    tutor_reference_code = Column(String(64))
    created_on = Column(String(64))
    updated_on = Column(String(64))

class Emails(Base):
    __tablename__ = 'content_emails'
    id = Column(Integer, primary_key=True)
    email_message = Column(Text)
    email_subject = Column(Text)
    email_name = Column(Text)
    email_status = Column(String(26))
    email_slug = Column(String(26))


class PricingTable(Base):
    __tablename__ = 'pricing'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    description = Column(Text)
    slug = Column(String(26))
    monthly = Column(String(10))
    quarterly = Column(String(10))
    yearly = Column(String(10))
    amount = Column(String(10))
    status = Column(Boolean, default=True)
    pricing_type = Column(String(10))




# subscriptions


class AdvisorSubscriptions(Base):
    __tablename__ = 'advisor_subscriptions'
    id = Column(Integer, primary_key=True)
    #pricing_id = Column(Integer, ForeignKey('pricing.id'))
    user_id = Column(Integer, ForeignKey('auth_user.id'))
    quantity = Column(String(26))
    amount = Column(String(26))
    transaction_status = Column(String(26))
    paypal_status = Column(String(26))
    subscription_date = Column(String(246))
    mode = Column(String(26))
    subscription_code = Column(String(46))
    paypal_return_data = Column(Text)
    subscription_to = Column(String(46))
    subscription_from = Column(String(46))
    subscription_items_meta = Column(Text)
    paypa_txn_id = Column(Text)
    paypal_verify_sign = Column(Text)
    user_uuid = Column(Text)




# transactions


class AdvisorTransactions(Base):
    __tablename__ = 'advisor_transactions'
    id = Column(Integer, primary_key=True)
    resource_id = Column(Integer, ForeignKey('pricing.id'))
    user_id = Column(Integer, ForeignKey('auth_user.id'))
    subscription_id = Column(Integer)
    quantity = Column(String(26))
    amount = Column(String(26))
    transaction_status = Column(String(26))
    paypal_status = Column(String(26))
    transaction_date = Column(String(56))
    mode = Column(String(26))
    subscription_code = Column(String(116))
    paypal_return_data = Column(Text)
    transaction_to = Column(String(46))
    transaction_from = Column(String(46))
    transaction_paypal = Column(Text)


class AccountantTransactions(Base):
    __tablename__ = 'accountant_transactions'
    id = Column(Integer, primary_key=True)
    resource_id = Column(Integer, ForeignKey('pricing.id'))
    user_id = Column(Integer, ForeignKey('auth_user.id'))
    subscription_id = Column(Integer)
    quantity = Column(String(26))
    amount = Column(String(26))
    transaction_status = Column(String(26))
    paypal_status = Column(String(26))
    transaction_date = Column(String(56))
    mode = Column(String(26))
    subscription_code = Column(String(116))
    paypal_return_data = Column(Text)
    transaction_to = Column(String(46))
    transaction_from = Column(String(46))
    transaction_paypal = Column(Text)





class SiteBlogs(Base):
    __tablename__ = 'blog_blogpost'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    status = Column(Integer)
    title = Column(Text)
    description = Column(Text)
    content = Column(Text)
    created = Column(String(50))
    updated = Column(String(50))
    rating_sum = Column(Integer)
    # this is used as status_type
    in_sitemap = Column(Boolean, nullable=False, default=True)
    gen_description = Column(Boolean, nullable=False, default=True)
    allow_comments = Column(Boolean, nullable=False, default=True)
    rating_count = Column(Integer)
    keywords_string = Column(Text)
    comments_count = Column(Integer)
    site_id = Column(Integer)
    rating_average = Column(Integer)

    @property
    def created_date(self):
        # return self.num_shares * self.price
        created_date = self.created.split(" ")
        return created_date[0]