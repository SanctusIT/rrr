import math
import time
from flask import abort, Blueprint, session, redirect, render_template, request, url_for
from passlib.hash import sha256_crypt
from sqlalchemy.orm.exc import NoResultFound
from database import db_session
import emails
import uuid
from .models import *
from flask_login import login_user, logout_user, current_user, login_required
from flask_login import current_user
from passlib.hash import django_pbkdf2_sha256
from .forms import LoginForm, RegistrationForm
from flask import jsonify
import sqlalchemy.exc
from time import time
import helpers as hlp
from hashlib import sha256
import hashlib
#from admin.models import StockCompanies, IriskawareRISKTable, IriskawareDISTTable, SoftwareSettingss
from datetime import date
from dateutil.relativedelta import relativedelta
import csv
import os
import random
from time import gmtime, strftime
import traceback
import datetime as dttim
from sqlalchemy.exc import SQLAlchemyError

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, '../static/')
blueprint = Blueprint('simple', __name__)



@blueprint.route('/tableCreate', methods=['POST'])
def create_table_details():
    if request.method == 'POST':
        advisor_id = None
        dict = request.form
        for key in dict.keys():
            for value in dict.getlist(key):
                print (key, ":", value)
        print (request.form['table_name'])
        if request.form.get('email') == "":
            return jsonify({'message': 'Email is required'})
        if request.form.get('email') != "":
            check_email = User.query.filter_by(
                email=request.form.get('email')).first()
            if check_email:
                return jsonify({'message': 'Email Already Exisits'})
        if request.form['table_name'] == "register":
            if request.form.get('email') != "":
                email = request.form.get('email')
                #hashes = sha256(str(email) + str(int(time()))).hexdigest()


                password = django_pbkdf2_sha256.encrypt(
                    request.form.get('password'))
                print(password)
                result_items = User(
                    username=email,
                    email=email,
                    first_name=request.form.get('first_name'),
                    last_name=request.form.get('last_name'),
                    password=password,
                    last_login=dttim.datetime.now(),
                    is_superuser=False,
                    is_staff=False,
                    is_active=True,
                    date_joined=dttim.datetime.now())
                db_session.add(result_items)
                db_session.commit()
                # db_session.flush()
                if result_items:
                    print (result_items.id)
                    hashes = sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()

                    print ("activation code ==", hashes)
                    if request.form.get('user_type') == "agent":
                        print("agent")
                        userprofile_items = UserProfile(
                            user_id=result_items.id,
                            phone_number=request.form.get('phone'),
                            zip_code=request.form.get('zip_code'),
                            city=request.form.get('city'),
                            state=request.form.get('state'),
                            home_address=request.form.get('home_address'),
                            alternate_phone=request.form.get('alternate_phone'),
                            postcode=request.form.get('zip_code'),
                            activation_code=hashes,
                            user_type=request.form.get('user_type'),
                            advisor_id=advisor_id,
                            license_number = request.form.get('license_number'),
                            license_name = request.form.get('license_name'),
                            license_date=request.form.get('license_date'),
                            license_state = request.form.get('license_state'),
                            #afsl=request.form.get('afsl'),
                            afsl_activate=False)
                    else:
                        print ("not advisor")
                        userprofile_items = UserProfile(
                            user_id=result_items.id,
                            company=request.form.get('company'),
                            zip_code=request.form.get('zip_code'),
                            city=request.form.get('city'),
                            postcode=request.form.get('zip_code'),
                            activation_code=hashes,
                            registration_number=request.form.get('registration_number'),
                            registration_body=request.form.get('registration_body'),
                            user_type=request.form.get('user_type'),
                            advisor_id=advisor_id,
                            afsl=request.form.get('afsl'),
                            afsl_activate=True,
                            accountant_id=request.form.get('regnnum'))

                    db_session.add(userprofile_items)
                    db_session.commit()
                    emails.user_register_email(result_items, hashes)
                    result = jsonify({'message': 'SUCCESS'})

                    '''
                    try:
                        db_session.add(userprofile_items)
                        db_session.commit()
                        emails.user_register_email(result_items, hashes)
                        result= jsonify({'message': 'SUCCESS'})
                    except:
                        db_session.rollback()
                        print "problem in inserting so rollback"
                        return jsonify({'message': 'Problem in creating user'})
                    '''
            else:
                return jsonify({'message': 'ERROR'})
        elif request.form['table_name'] == "refer_client":
            if request.form.get('first_name') != "" and request.form.get('email') != "":

                result_items = UserClients(
                    user_id=current_user.id,
                    client_email=request.form.get('email'),
                    client_first_name=request.form.get('first_name'),
                    client_last_name=request.form.get('last_name'),
                    client_city=request.form.get('city'),
                    client_state=request.form.get('state'),
                    client_comments=request.form.get('comments'),
                    client_zipcode=request.form.get('zip_code'),
                    client_reason=request.form.get('reason'),
                    client_address=request.form.get('home_address'),
                    client_buy_sell=request.form.get('buy_sell'),
                    updated_on=dttim.datetime.now(),
                    is_active=True,
                    created_on=dttim.datetime.now())
                db_session.add(result_items)
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
            else:
                return jsonify(
                        {'message': 'ERROR', 'RESPONSE': 'Something went wrong!'})

        else:
            return jsonify({'message': 'ERROR'})

        return result
    else:
        return jsonify({'message': 'Unsupported Request'})


@blueprint.route('/clientCRUD', methods=['POST'])
@login_required
def client_table_crud():
    if request.method == 'POST':
        advisor_id = None
        dict = request.form
        for key in dict.keys():
            for value in dict.getlist(key):
                print (key, ":", value)
        print (request.form['crud_type'])
        if request.form['crud_type'] == "updateprofile":
            if request.form.get(
                    'module') != "" and request.form.get('uid') != "":
                user = User.query.get(current_user.id)
                user.first_name = request.form.get('first_name')
                user.last_name = request.form.get('last_name')
                db_session.commit()
                # db_session.flush()
                print (current_user.id)
                userprofile = UserProfile.query.filter_by(
                    user_id=current_user.id).first()
                userprofile.company = request.form.get('company')
                userprofile.phone_number = request.form.get('phone')
                userprofile.city = request.form.get('city')
                #city = request.form.get('city'),
                userprofile.state = request.form.get('state'),
                userprofile.home_address = request.form.get('home_address'),
                userprofile.alternate_phone = request.form.get('alternate_phone'),
                userprofile.zip_code = request.form.get('zip_code')
                userprofile.postcode = request.form.get('zip_code')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        elif request.form['crud_type'] == "update_license":
            print("dfsf")
            if request.form.get('user_type') == "agent":
                userprofile = UserProfile.query.filter_by(user_id=current_user.id).first()
                userprofile.license_number = request.form.get('license_number')
                userprofile.license_name = request.form.get('license_name')
                userprofile.license_date = request.form.get('license_date')
                userprofile.license_state = request.form.get('license_state')
                db_session.commit()
                result = jsonify({'message': 'SUCCESS'})
        else:
            return jsonify({'message': 'ERROR'})

        # print type(result)
        return result
    else:
        return jsonify({'message': 'Unsupported Request'})


@blueprint.route('/thankyou')
def thankyou():
    return render_template('thankyou.html')


@blueprint.route('/test_mail')
def test_mail():
    user = User.query.filter_by(email='hughson.simon@gmail.com').first()

    print (user.first_name)
    emails.test_email("subject here", user, "sdff")
    return "test mail"


@blueprint.route('/')
def home():
    try:
        '''
        home_page_header = PageBlocks.query.filter_by(
            block_slug="header").first()
        banners = PageBlocks.query.filter_by(block_slug="banners").all()
        services_iriskonline = PageBlocks.query.filter_by(
            block_slug="services_iriskonline").first()
        services_iriskaware = PageBlocks.query.filter_by(
            block_slug="services_iriskaware").first()
        services_iriskwatch = PageBlocks.query.filter_by(
            block_slug="services_iriskwatch").first()
        '''
        home_page_header=""
        banners=""
        services_iriskonline=""
    except BaseException:
        db_session.rollback()
        return redirect('/logout')
    return render_template(
        'index.html',
        title="Realty Referral Resource ",
        home_page_header=home_page_header,
        banners="",
        services_iriskonline="",
        services_iriskwatch="",
        services_iriskaware="")


@blueprint.route('/services')
def homeServices():
    services_iriskonline = PageBlocks.query.filter_by(
        block_slug="services_iriskonline").first()
    services_iriskaware = PageBlocks.query.filter_by(
        block_slug="services_iriskaware").first()
    services_iriskwatch = PageBlocks.query.filter_by(
        block_slug="services_iriskwatch").first()
    return render_template('services.html',
                           services_iriskonline=services_iriskonline,
                           services_iriskwatch=services_iriskwatch,
                           services_iriskaware=services_iriskaware)


@blueprint.route('/contact', methods=['GET', 'POST'])
def homeContact():
    if request.method == 'POST':
        first_name=request.form.get('first_name')
        last_name = request.form.get('last_name')
        name=first_name+last_name
        message = '''
        Hi,

        Message from website:

        Name: %s
        Email: %s
        phone: %s
        Message:
        %s

        Cheers,
        Support team
        ''' % (name,
               request.form.get('email'),
               request.form.get('number'),
               request.form.get('message'))
        emails.send_email("Message from website contact us page",
                          ["suzanne@realtyreferralresource.com"],
                          message )
        return jsonify({'message': 'SUCCESS'})
    return render_template('contact.html')


@blueprint.route('/send-a-referral')
def send_a_referral():
    faqs_records = Pages.query.filter_by(page_slug="aboutus").first()
    return render_template(
        'coming_soon.html',
        #'send-a-referral.html',
        faqs_records=faqs_records,
        title="Realty Referral Resource : Send a Referral")

@blueprint.route('/continueEdu')
def continueedu():
    faqs_records = Pages.query.filter_by(page_slug="aboutus").first()
    return render_template(
        'continueEdu.html',
        faqs_records=faqs_records,
        title="Realty Referral Resource : continueEdu")


@blueprint.route('/about')
def about_us():
    faqs_records = Pages.query.filter_by(page_slug="aboutus").first()
    return render_template(
        'about_us.html',
        faqs_records=faqs_records,
        title="Realty Referral Resource : About Us")

@blueprint.route('/blog')
def site_blogs():
    blog_records = SiteBlogs.query.filter_by(status=2)
    latest_records = SiteBlogs.query.filter_by(status=2).order_by("id desc").limit(7)
    print (latest_records.count())
    return render_template(
        'blog.html',
        blog_records=blog_records,
        latest_records = latest_records,
        title="Iriskonline : Blogs")

@blueprint.route('/viewblog', methods=['GET', 'POST'])
def view_site_blogs():
    print(request.args.get('bid'))
    blog_records = SiteBlogs.query.filter_by(id=request.args.get('bid')).first()
    print(blog_records.title)
    latest_records = SiteBlogs.query.filter_by(status=2).order_by("id desc").limit(7)
    return render_template(
        'blog-single.html',
        blog_records=blog_records,
        latest_records = latest_records,
        title="Iriskonline : Blogs")

@blueprint.route('/faqs')
def faqs():
    faqs_records = Faqs.query.filter_by(faq_status=True).all()
    return render_template(
        'faqs.html',
        faqs_records=faqs_records,
        title="Realty Referral Resource: Frequently Asked Questions")


@blueprint.route('/activate_account', methods=['GET'])
def activate_account():
    error = ""
    if request.args.get('hashes'):
        check_hashes = UserProfile.query.filter_by(
            activation_status=False,
            activation_code=request.args.get('hashes')).first()
        if check_hashes:
            check_hashes.activation_status = True
            db_session.commit()
        else:
            error = "Invalid request. Please contact administrator"
    else:
        error = "Invalid Token request"
    return render_template(
        'activate_account.html',
        error=error,
        title="Email Activated")


@blueprint.route('/createclient')
@login_required
def createClient():
    register_form = RegistrationForm(csrf_enabled=True)
    return render_template(
        'create_client_form.html',
        register_form=register_form)




'''
formsd = {'item_number': 'a62fb6fb-1523-4828-8b73-2e55b891adff', 'protection_eligibility': 'Eligible',
              'receiver_id': 'UBD5YLHJ4VRTA', 'last_name': 'buyer', 'txn_id': '9F3253899B0829643',
              'transaction_subject': '', 'business': 'hughsons.apps_merchant@gmail.com', 'address_country_code': 'IN',
              'receiver_email': 'hughsons.apps_merchant@gmail.com', 'payment_status': 'Completed',
              'payment_gross': '250.00', 'mc_fee': '10.05', 'tax': '0.00', 'payer_id': '94JX2KYWAVWGJ',
              'residence_country': 'IN', 'address_state': 'Maharashtra', 'payer_status': 'verified',
              'txn_type': 'web_accept', 'verify_sign': 'AgePMPfbDDc05TM5uouG2O9s9U74AUaynxDLguOidDP6yjst-oD6ThU.',
              'address_street': 'Flat no. 507 Wing A Raheja Residency\r\nFilm City Road, Goregaon East',
              'handling_amount': '0.00', 'address_zip': '400097', 'payment_fee': '10.05',
              'payment_date': '22:08:03 Nov 27, 2016 PST', 'first_name': 'test', 'address_status': 'unconfirmed',
              'address_city': 'Mumbai', 'item_name': 'Iriskonline Resource Payment', 'address_country': 'India',
              'charset': 'windows-1252', 'mc_currency': 'USD', 'shipping': '0.00', 'custom': 'IRISKWAREREPORT',
              'payer_email': 'hughson.simon-buyer@gmail.com', 'notify_version': '3.8', 'payment_type': 'instant',
              'address_name': 'test buyer', 'mc_gross': '250.00', 'ipn_track_id': '7d990461e6538', 'test_ipn': '1', 'quantity': '1'}
    get_subscription = AdvisorSubscriptions.query.filter_by(subscription_code='a62fb6fb-1523-4828-8b73-2e55b891adff').first()
    activate_transaction("SUCCESS", get_subscription.id, forms)
'''

@blueprint.route('/dashboard')
@login_required
def dashboard():
    #print current_user.userid.user_type
    if current_user.userid.user_type == "admin":
        return redirect('/logout')
    advisor_clients = ""
    client_portfolios = ""
    if current_user.userid.user_type == "advisor":
        #advisor_clients = User.query.filter(UserProfile.advisor_id == current_user.get_id)
        advisor_clients = User.query.join(
            UserProfile, User.id == UserProfile.user_id).filter(
            UserProfile.advisor_id == current_user.id)

    return render_template('dashboard.html',
                           advisor_clients=advisor_clients,
                           client_portfolios=client_portfolios)

@blueprint.route('/myprofile')
@login_required
def my_profile():
    print(current_user.id)
    error = ""
    register_form = RegistrationForm(csrf_enabled=True)
    client_portfolios = UserClients.query.filter(UserClients.user_id == current_user.id)

    trxn_id = int(hashlib.md5(current_user.email.encode('utf-8')).hexdigest(), 16)
    print(trxn_id)
    count_transaction = AdvisorSubscriptions.query.filter_by(user_uuid=str(trxn_id)).count()
    if count_transaction >= 1:
        template_name = 'myprofile.html'
    else:
        template_name = 'first_payment.html'
    return render_template(
        template_name,
        trxn_id=trxn_id,
        client_portfolios=client_portfolios,
        register_form=register_form,
        error=error)

@blueprint.route('/myaccount')
@login_required
def myaccount():
    print(current_user.id)
    error = ""
    client_portfolios = UserClients.query.filter(UserClients.user_id == current_user.id)
    template_name='first_payment.html'
    trxn_id = int(hashlib.md5(current_user.email.encode('utf-8')).hexdigest(), 16)
    print(trxn_id)
    count_transaction = AdvisorSubscriptions.query.filter_by(user_uuid=str(trxn_id)).count()
    if count_transaction>=1:
        template_name = 'my-account.html'
    else:
        template_name = 'first_payment.html'
    return render_template(
        #'my-account.html',
        template_name,
        trxn_id=trxn_id,
        client_portfolios=client_portfolios,
        error=error)

@blueprint.route('/refer_client')
@login_required
def refer_client():
    print(current_user.userid.user_type)
    error = ""
    register_form = RegistrationForm(csrf_enabled=True)
    return render_template(
        'coming_soon.html',
        #'refer_client.html',
        register_form=register_form,
        error=error)


@blueprint.route('/payments')
@login_required
def payments():
    #print(current_user.id)
    error = ""


    trxn_id = int(hashlib.md5(current_user.email.encode('utf-8')).hexdigest(), 16)
    #print(trxn_id)
    count_transaction = AdvisorSubscriptions.query.filter_by(user_uuid=str(trxn_id)).count()
    if count_transaction >= 1:
        template_name = 'payments.html'
    else:
        template_name = 'first_payment.html'
    client_portfolios = AdvisorSubscriptions.query.filter_by(user_uuid=str(trxn_id)).all()
    #print(len(client_portfolios))
    return render_template(
        template_name,
        trxn_id=trxn_id,
        client_portfolios=client_portfolios,
        error=error)


@blueprint.route('/license')
@login_required
def license():
    error = ""
    register_form = RegistrationForm(csrf_enabled=True)

    trxn_id = int(hashlib.md5(current_user.email.encode('utf-8')).hexdigest(), 16)
    #print(trxn_id)
    count_transaction = AdvisorSubscriptions.query.filter_by(user_uuid=str(trxn_id)).count()
    if count_transaction >= 1:
        template_name = 'license_information.html'
    else:
        template_name = 'first_payment.html'
    return render_template(
        template_name,
        trxn_id=trxn_id,
        register_form=register_form,
        error=error)

@blueprint.route('/forgot-password', methods=['GET', 'POST'])
def forgot_password():
    if request.method == 'POST':
        user = None
        try:
            print(request.form.get('email'))
            email=request.form.get('email')
            #email.encode('utf-8')
            user = User.query.filter_by(
                email=email).first()

            if user:
                #email=user.email
                email_id = str(email) + str(int(time()))
                password = hlp.randomString(11)
                password_hash = django_pbkdf2_sha256.encrypt(password)
                user = User.query.get(user.id)
                user.password = password_hash
                #user.last_name = request.form.get('last_name')
                db_session.commit()
                #hashes = sha256(email_id.encode('utf-8')).hexdigest()
                emails.password_reset(user, password)
                return jsonify({'message': 'SUCCESS'})
            else:
                return jsonify({'message': 'ERROR'})
        except NoResultFound:
            return jsonify({'message': 'ERROR'})
        return render_template(
            'forgot_password_sent.html',
            email=user.person.email if user else '')
    return render_template('forgot_password.html')


@blueprint.route('/reset_password', methods=['GET', 'POST'])
def reset_password():
    reset = PasswordReset.query.get(hash)
    if not reset:
        abort(404)
    if request.method == 'POST':
        reset.user.password = sha256_crypt.encrypt(request.form['password'])
        db_session.delete(reset)
        db_session.commit()
        if register:
            template = 'register_success.html'
        else:
            template = 'reset_password_success.html'
        return render_template(template, person=reset.user.person)
    if register:
        template = 'register.html'
    else:
        template = 'reset_password.html'
    return render_template(template, person=reset.user.person)


@blueprint.route('/register', methods=['GET', 'POST'])
def register():
    faqs_records = Pages.query.filter_by(page_slug="get_started").first()
    register_form = RegistrationForm(csrf_enabled=True)
    return render_template("joinus.html",
                           register_form=register_form,
                           faqs_records=faqs_records)


def user_online_status(id, status):
    update_record = UserProfile.query.get(id)
    update_record.is_loggedin = status
    db_session.commit()


@blueprint.route('/login', methods=['GET', 'POST'])
def login():

    error = None
    login_form = LoginForm(csrf_enabled=True)
    if request.method == 'POST':
        # print request.form['email']
        emailid, password = request.form['email'].lower(
        ), request.form['password']
        #print(emailid)
        #emailid = emailid.encode('ascii', 'ignore')
        #password = password.encode('ascii', 'ignore')
        try:
            #print(emailid)
            check_email = User.query.filter_by(email=str(emailid)).first()
            # print user.id
        except NoResultFound:
            error = "No user with that email address exists."

        if check_email:
            # print "user type", check_email.is_active
            user = User.query.filter_by(email=emailid, ).first()
            # print user.id
            if user.is_active():
                if user.userid.activation_status:
                    remember_me = False
                    if 'remember_me' in request.form:
                        remember_me = True
                    if user.password:
                        if django_pbkdf2_sha256.verify(
                                str(password), user.password):
                            session['user_id'] = user.id
                            login_user(user, remember=remember_me)
                            #user_online_status(current_user.userid.id, "True")
                            return hlp.redirect_next('/payments')
                        else:
                            error = "Incorrect password."
                    else:
                        error = "You have not set up a password yet. Use the 'Forgot Password' link below to reset it."
                else:
                    error = "Your email is not activated yet"
            else:
                error = "User with this email address is inactive."
        else:
            error = "No user with that email address exists."
    else:
        user = current_user
        user.authenticated = False
        logout_user()
        session.clear()
        logout_user()
    return render_template('signin.html', error=error, login_form=login_form)


@blueprint.route('/return_advisor', methods=['GET', 'POST'])
def paypal_return_advisor():
    # print current_user.userid.user_type
    error = ""
    #register_form = RegistrationForm(csrf_enabled=True)
    return render_template('paypal_success.html', error=error)


@blueprint.route('/cancel_request_advisor')
@login_required
def cancel_request_advisor():
    f = request.form
    for key in f.keys():
        for value in f.getlist(key):
            print(key, ":", value)
    emails.notify_url_email(f)
    forms = {}
    for keyd in f.keys():
        for value in f.getlist(keyd):
            # print keyd,":",value
            forms.update({keyd: str(value)})
    error = ""
    # print "*" * 29
    # print forms
    # print "*" * 29
    #emails.notify_url_email(forms)
    return render_template('cancel_request_advisor.html')



@blueprint.route('/notify_url_advisor', methods=['GET', 'POST'])
def notify_url_advisor():
    f = request.form
    forms = {}
    for keyd in f.keys():
        for value in f.getlist(keyd):
            # print keyd,":",value
            forms.update({keyd: str(value)})
    error = ""
    #print "*" * 29
    #print forms
    #print "*" * 29
    #emails.notify_url_email(forms)
    #get_paypal_txn = AdvisorSubscriptions.query.filter_by(paypa_txn_id=request.form.get('txn_id')).count()
    #print(get_paypal_txn)
    #if get_paypal_txn >= 1:
    #    emails.notify_url_email("transaction already exist")
        #return "transaction already exist"

    if 'custom' in request.form and 'item_number' in request.form:
        insert_into = AdvisorSubscriptions(
            paypal_return_data=str(forms),
            paypa_txn_id=request.form.get('txn_id'),
            transaction_status=request.form.get('payment_status'),
            user_uuid=request.form.get('item_number'),
            amount=request.form.get('mc_gross'),
            subscription_date=dttim.datetime.now(),
            paypal_verify_sign=request.form.get('verify_sign'),
            subscription_code=request.form.get('payer_id'))
        db_session.add(insert_into)
        db_session.commit()
        emails.notify_url_email(" inserting the payment record success")
    else:
        emails.notify_url_email("problem in inserting the payment record")

    return jsonify({'message': 'paypal notification url'})


@blueprint.route('/logout')
def logout():
    """Logout the current user."""
    try:
        user = current_user
        # print current_user.userid.id
        #user_online_status(current_user.userid.id, "False")
        user.authenticated = False
        logout_user()
        session.clear()
        logout_user()
    except Exception as e:
        print(e)
        # return e
    return redirect(url_for('simple.home'))
