import sys

from database import db_session as db, init_db
from models import Setting


def init_settings():
    fields = [
        'aed_expiration_alert',
        'personnel_expiration_alert',
        'inspection_alert',
        'event_alert',
        'readitrack_invite',
        'distributor_invite',
        'entity_invite',
        'past_due_inspection_report',
        'email_footer']
    for field in fields:
        db.add(Setting(name=field, value=''))
    db.commit()


if __name__ == '__main__':
    populate = False
    if len(sys.argv) > 1 and sys.argv[1] == 'populate':
        populate = True
    init_db(populate)
    init_settings()
