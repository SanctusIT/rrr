__author__ = 'hughson.simon@gmail.com'

from flask_login import LoginManager

login_manager = LoginManager()

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
