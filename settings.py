from datetime import timedelta
import os

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
BCRYPT_LOG_ROUNDS = 13

ASSETS_DEBUG = False
DEBUG_TB_ENABLED = False  # Disable Debug toolbar
DEBUG_TB_INTERCEPT_REDIRECTS = False
LOG_RESPONSE = True

CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
WTF_CSRF_ENABLED = False

SQLALCHEMY_COMMIT_ON_TEARDOWN = True


ADMINS = ['rrr@gmail.com', 'hughson.simon@gmail.com']
MANAGERS = ADMINS

#SERVER_NAME = 'http://127.0.0.1:5000/'

DEBUG = False
TESTING = False

DB = {
    'host': '52.40.177.20:5432',
    'user': 'rrrdbuser',
    'password': 'rrr123*',
    'database': 'rrr_project',
}

Databases = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': '52.40.177.20',
                'NAME': 'rrr_project',
                'PASSWORD': 'rrr123*',
                'PORT': 5432,
                # Set to empty string for default. Not used with sqlite3.
                'USER': 'rrrdbuser'
    }
}

PERMANENT_SESSION_LIFETIME = timedelta(minutes=120)
SECRET_KEY = '\xcb\xe2\xaf%\xaeV(\xf9S\xa1\xa1\x90\xf5\xfd>\xdey\x18\x8f04\xfa\xed\x84'

MAIL_SERVER = 'mail.smtp2go.com'
MAIL_PORT = 2525  # 8025, 587 and 25 can also be used.
MAIL_USE_TSL = True
MAIL_USERNAME = 'james@realtyreferralresource.com'
MAIL_PASSWORD = 'Realty@123'
MAIL_DEFAULT_SENDER = 'support@realtyreferralresource.com'

STRIPE_PUBLIC_KEY = 'pk_live_45ReJuOvb13kbGNZMOWH387Q'
STRIPE_SECRET_KEY = 'sk_live_pGUCOxgW15L6xqxv7ezeKcZH'


INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.humanize",
    "django.contrib.redirects",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.sitemaps",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "analysis"


)
